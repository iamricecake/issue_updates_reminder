# Issue Updates Reminder

When slack reminders are not enough.

This project allows a member to schedule a pipeline that would run the job for their GitLab.com account and check for assigned issues within the `gitlab-org` group
that have not been updated with a comment that uses the Verify::Testing Group's [issue update format](https://about.gitlab.com/handbook/engineering/development/ops/verify/testing/#issue-progress-updates).

The script only detects updates that start with `## Update` for now. If the issue has not been updated for 3 days or longer from the time the pipeline runs, a new issue will be created in this project assigned to the pipeline owner containing a checklist of issues to update.

## How to add a scheduled pipeline for yourself

1. Go to [Pipeline Schedules](https://gitlab.com/iamricecake/issue_updates_reminder/-/pipeline_schedules).
1. Add a new schedule.
1. Target the `master` branch.
1. Add `ACCESS_TOKEN` variable and enter your personal access token with read/write API permission.
1. Set your desired cron schedule then save.
