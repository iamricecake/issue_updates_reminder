const { GraphQLClient, gql } = require('graphql-request')
const differenceInDays = require('date-fns/differenceInDays')
const formatDistanceToNow = require('date-fns/formatDistanceToNow')
const parseISO = require('date-fns/parseISO')

const UPDATE_START_PATTERN = '## Update'
const DAYS_TOO_OLD = 3
const ENDPOINT = 'https://gitlab.com/api/graphql'
const GQL_CLIENT = new GraphQLClient(ENDPOINT, {
  headers: {
    authorization: `Bearer ${process.env.ACCESS_TOKEN}`, // my token with read_api access
  },
})

function isUpdateOld(lastUpdateAt) {
  return differenceInDays(new Date, parseISO(lastUpdateAt)) >= DAYS_TOO_OLD
}

function noteUrl(issueUrl, noteId) {
  return `${issueUrl}#note_${noteId}`
}

function createIssue(issuesToUpdate, projectPath, userId) {
  const checklist = issuesToUpdate.map(i => `- [ ] \`${i.issue.webUrl}\` (last update: ${formatDistanceToNow(parseISO(i.lastUpdate.createdAt))})` )

  const description = `
## Issues to update

${checklist.join("\n")}
`

  const query = gql`
    mutation CreateIssue($input: CreateIssueInput!) {
      createIssue(input: $input) {
        issue {
          id
        }
      }
    }
  `

  const variables = {
    input: {
      title: "Issue Updates Reminder",
      description: description.trim(),
      assigneeIds: [`gid://gitlab/User/${userId}`],
      projectPath: projectPath,
    },
  }

  return GQL_CLIENT.request(query, variables)
}

function fetchIssues(username) {
  const groupName = 'gitlab-org'
  const issueState = 'opened'
  const query = gql`
    {
      group(fullPath: "${groupName}") {
        issues(assigneeUsername: "${username}", state: ${issueState}) {
          edges {
            node {
              id
              title
              webUrl
              milestone {
                title
                dueDate
              }
              discussions {
                edges {
                  node {
                    id
                    notes {
                      edges {
                        node {
                          id
                          body
                          createdAt
                          author {
                            id
                            username
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `

  return GQL_CLIENT.request(query)
}

function processIssuesToUpdate(data, username) {
  const issuesToUpdate = []

  data.group.issues.edges.forEach(i => {
    const { milestone, discussions } = i.node
    let lastUpdate

    discussions.edges.forEach(d => {
      d.node.notes.edges.forEach(n => {
        const { body, author } = n.node

        if (author.username == username && body.startsWith(UPDATE_START_PATTERN)) {
          lastUpdate = n.node
        }
      })
    })

    // Making an assumption here that if dueDate present then it is the current milestone
    // and we only want to remind about updates for the current milestone.
    if (milestone && milestone.dueDate && isUpdateOld(lastUpdate.createdAt)) {
      issuesToUpdate.push({
        issue: i.node,
        lastUpdate,
      })
    }
  })

  return issuesToUpdate
}

async function main() {
  const projectPath = process.env.CI_PROJECT_PATH // taken from predefined CI/CD variable
  const username = process.env.GITLAB_USER_LOGIN // taken from predefined CI/CD variable
  const userId = process.env.GITLAB_USER_ID // taken from predefined CI/CD variable

  const data = await fetchIssues(username)
  const issuesToUpdate = processIssuesToUpdate(data, username)

  if (issuesToUpdate.length) {
    const createdIssue = await createIssue(issuesToUpdate, projectPath, userId)
    console.log(createdIssue)
  } else {
    console.log('No issues to update.')
  }
}

main().catch((error) => console.error(error))
